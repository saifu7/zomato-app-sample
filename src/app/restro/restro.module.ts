import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestrohomeComponent } from './restrohome/restrohome.component';
import { OrdersComponent } from './orders/orders.component';
import { MenuComponent } from './menu/menu.component';

import { RouterModule, Routes } from '@angular/router';

var routes : Routes = 
[
  {path : 'home', component: RestrohomeComponent},
  {path : 'orders', component: OrdersComponent},
  {path : 'Menu', component: MenuComponent}
]

@NgModule({
  declarations: [RestrohomeComponent, OrdersComponent, MenuComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class RestroModule { }
