import { Component, OnInit } from '@angular/core';

import { FormControl, Validators, FormGroup } from '@angular/forms';

import { RestaurantService } from '../restaurant.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-restosignup',
  templateUrl: './restosignup.component.html',
  styleUrls: ['./restosignup.component.css']
})
export class RestosignupComponent implements OnInit {

  constructor(private restSer:RestaurantService,  private router:Router) { }

  ngOnInit() {
  }

  restroSignupForm  = new FormGroup({
    name  : new FormControl('', Validators.required),
    city  : new FormControl('', Validators.required),
    mobile  : new FormControl('', Validators.required),
    email  : new FormControl('', Validators.required),
    password  : new FormControl('', Validators.required),
  });

  signup()
  {
    console.log(this.restroSignupForm.value);
    this.restSer.addRestaurant(this.restroSignupForm.value).subscribe(success => {
      console.log(success);
      // this.router.navigateByUrl('/login/');
    }, error => {
      console.log(error);
    });
  }
}
