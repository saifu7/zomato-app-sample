import { Component, OnInit } from '@angular/core';


import { RestaurantService } from '../../restaurant.service';
import { CustomerhomeComponent } from '../customerhome/customerhome.component';
import { Router } from '@angular/router';
@Component({
  selector: 'app-restromenu',
  templateUrl: './restromenu.component.html',
  styleUrls: ['./restromenu.component.css']
})
export class RestromenuComponent implements OnInit {

  constructor(private restSer:RestaurantService,  private router:Router) { }
  menus =  [
              {
                "rest_id": 1,
                "name" : "Pizza",
                "cost" : 150
              },
              {
                "rest_id" : 3,
                "name" : "Rice ball",
                "cost" : 60
              }
  ];
  ngOnInit() {
    
  }

  addOrder()
  {
    
  }
  
}
