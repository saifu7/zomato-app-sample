import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestromenuComponent } from './restromenu.component';

describe('RestromenuComponent', () => {
  let component: RestromenuComponent;
  let fixture: ComponentFixture<RestromenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestromenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestromenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
