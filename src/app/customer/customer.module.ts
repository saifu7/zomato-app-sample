import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerhomeComponent } from './customerhome/customerhome.component';
import { RestrodetailsComponent } from './restrodetails/restrodetails.component';
import { RestromenuComponent } from './restromenu/restromenu.component';

import { RouterModule, Routes } from '@angular/router';

var routes : Routes =
[
  {path : '' , redirectTo : 'home', pathMatch : 'full'},
  {path : 'home', component : CustomerhomeComponent},
  {path : 'restrodetails', component : RestrodetailsComponent},
  {path : 'restro-menu', component : RestromenuComponent}
];

@NgModule({
  declarations: [CustomerhomeComponent, RestrodetailsComponent, RestromenuComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class CustomerModule { }
