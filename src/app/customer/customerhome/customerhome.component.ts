import { Component, OnInit } from '@angular/core';
import {RestaurantService} from '../../restaurant.service';


import { Router } from '@angular/router';
@Component({
  selector: 'app-customerhome',
  templateUrl: './customerhome.component.html',
  styleUrls: ['./customerhome.component.css']
})
export class CustomerhomeComponent implements OnInit {

  restaurant:any = [];
  constructor(private resSer: RestaurantService,   private router:Router) { }

  ngOnInit() {

    this.getAllRestros();
  }
  menus : any = [];
  getAllRestros()
  {
    this.resSer.getRestaurant().subscribe(success => {
      console.log(success);
      this.restaurant = success;
    }, error => {
      console.log(error);
    });
  }

  selectRestro(r)
  {
    this.resSer.fetchMenu(r.id).subscribe(success => {
      console.log(success);
      this.menus = success;
      this.router.navigateByUrl('/customer/restro-menu');
    },
    error => {
      console.log(error);
   });
    
  }
  
}
