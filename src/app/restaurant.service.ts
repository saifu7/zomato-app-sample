import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RestaurantService {

  constructor(private http: HttpClient) { }

  getRestaurant()
  {
    return this.http.get("http://localhost:3000/restaurant");
  }

  addRestaurant(s){
    console.log(s);    
    return this.http.post("http://localhost:3000/restaurant", s);
  }

  fetchMenu(id){
    console.log(id);
    return this.http.get(`http://localhost:3000/menus/${id}`);
  }
}
