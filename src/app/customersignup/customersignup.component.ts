import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomerService } from '../customer.service';
import {ActivatedRoute, Router} from '@angular/router';
@Component({
  selector: 'app-customersignup',
  templateUrl: './customersignup.component.html',
  styleUrls: ['./customersignup.component.css']
})
export class CustomersignupComponent implements OnInit {

  constructor(private cusSer:CustomerService,  private router:Router) { }

  ngOnInit() {
  }

  signupForm  = new FormGroup({
    name  : new FormControl('', Validators.required),
    mob  : new FormControl('', Validators.required),
    email  : new FormControl('', Validators.required),
    city  : new FormControl('', Validators.required),
    password  : new FormControl('', Validators.required)
  });
  
  signup()
  {
    console.log(this.signupForm.value);
    this.cusSer.addCustomer(this.signupForm.value).subscribe( success => {
      console.log(success);
      this.router.navigateByUrl('/login');
    }, error => {
      console.log(error);
    });
  }
      
}   
