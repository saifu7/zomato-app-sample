import { Component, OnInit } from '@angular/core';


import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import{ CustomerService} from '../customer.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router:Router, private custSer:CustomerService) { }

  ngOnInit() {
  }

  loginForm = new FormGroup({
		email  : new FormControl('', Validators.required),
    password  : new FormControl('', Validators.required)
  });
	
  login()
  {
    this.custSer.getByEmail(this.loginForm.value.email).subscribe(success => {
      console.log(success);
      let customer:any = success;
      if(customer.length == 1)
      {
        if(this.loginForm.value.password == customer[0].password)
        {
          alert('login successfule');
          this.router.navigateByUrl('/customer/home');
        }
      }
      else
      {
        alert("Check your email or password");
      }
    }, error => {
      console.log(error);
      alert("Check your email or password");
    });
  }

}
