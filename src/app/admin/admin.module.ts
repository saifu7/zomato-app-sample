import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminhomeComponent } from './adminhome/adminhome.component';
import { RestrosComponent } from './restros/restros.component';
import { UsersComponent } from './users/users.component';
import { AdminordersComponent } from './adminorders/adminorders.component';

import { RouterModule, Routes } from '@angular/router';

var routes : Routes = 
[
  {path : 'home', component : AdminhomeComponent },
  {path : 'users', component : UsersComponent },
  {path : 'restros', component : RestrosComponent },
  {path : '', redirectTo : 'home', pathMatch : 'full' },
];

@NgModule({
  declarations: [AdminhomeComponent, RestrosComponent, UsersComponent, AdminordersComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class AdminModule { }
