import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) { }

  getCustomer()
  {
    return this.http.get("http://localhost:3000/customer");
  }

  addCustomer(s){
    console.log(s);
    
    return this.http.post("http://localhost:3000/customer", s);
  }

  getByEmail(email)
  {
    // query params
    // url?key=value&key=value&key=value
    return this.http.get("http://localhost:3000/customer/getByEmail?email=" + email);
  }
}
